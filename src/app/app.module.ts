import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule, MatChipsModule, MatSelectModule,
  MatDatepickerModule, MatNativeDateModule, MatButtonModule,
  MatSidenavModule, MatToolbarModule, MatMenuModule, MatTreeModule,
  MatIconModule, MatFormFieldModule, MatListModule, MatExpansionModule,
  MatCardModule, MatAutocompleteModule, MatCheckboxModule, MatDialogModule, MatRadioModule, MatSnackBarModule, MatProgressBarModule
} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    BrowserAnimationsModule,
    MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
    MatSortModule, MatTableModule, MatChipsModule, MatSelectModule,
    MatDatepickerModule, MatNativeDateModule, MatButtonModule,
    MatSidenavModule, MatToolbarModule, MatMenuModule, MatTreeModule,
    MatIconModule, MatFormFieldModule, MatListModule, MatExpansionModule,
    MatCardModule, MatAutocompleteModule, MatCheckboxModule, MatDialogModule, MatRadioModule, MatSnackBarModule, MatProgressBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
