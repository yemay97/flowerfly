export interface Player {
    _id?: string;
    playerName: string;
    result: string;
    clearTime: number;
    answers?: string[];
    createdDate?: Date;
}
