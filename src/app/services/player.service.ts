import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrl } from '../../env';
import { Player } from '../models';

@Injectable({
    providedIn: 'root'
})
export class PlayerService {

    constructor(private http: HttpClient) { }

    getAllPlayers() {
        return this.http.get(`${apiUrl}/player`);
    }

    addPlayer(player: Player) {
        return this.http.post(`${apiUrl}/player`, player);
    }

}
