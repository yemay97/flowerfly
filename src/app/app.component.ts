import { Component } from '@angular/core';
import { PlayerService } from './services/player.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Player } from './models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FlowerFly';
  question: any;
  totalMarks = 0;
  number = 0;
  player: Player = { playerName: '', result: '', clearTime: 0, answers: [] };
  name = new FormControl('', [Validators.required, Validators.email]);
  startGame = false;
  allPlayers: any;
  angForm: FormGroup;
  displayMessage = '';
  time = 0;
  interval;
  wrongDisplay = ['Why you so noob one?', 'So weak, stop playing le la, go back work la',
    'Hi, this quiz is only for brain age above 3 years old, you are not qualified! Please ask accompanion from your daddy and mommy'];
  questions = [{
    question: 'Which animal is my favourite animal?',
    choices: ['Zebra', 'Penguin', 'Elephant', 'Chick', 'Puppy', 'Kitten'], correct: 'Puppy'
  },
  {
    question: 'Which is my favourite food?',
    choices: ['Spicy Ramen', 'Kimchi Ramen', 'Cheese Ramen', 'Soy Sauce Ramen', 'Pork Belly Ramen', 'Waffle'], correct: 'Waffle'
  },
  {
    question: 'How to spell my full name?',
    choices: ['Ooi Yi Mei', '0ui Ye May', 'Oo1 Ye May', 'Oi Yi Moi', 'Oai Ye May',
      'Aai Yi May', '0oi Ye May', 'Oo1 Ye May', 'Ooi Ye May', 'Ooooooi Ye May'],
    correct: 'Ooi Ye May'
  }, {
    question: 'How bored am I in office TODAY?',
    choices: ['Little', 'So-so', 'Super bored!!!',
      'Not at all, busy as hell! 玩玩阿你以为', 'Of course busying handle big project lar!', 'Sleepy'],
    correct: 'Of course busying handle big project lar!'
  },
  {
    question: 'Which of my eyes very blur?', choices: ['Left Eye', 'Right Eye', 'So pretty confirm both eye also shinning bright',
      'Everyday play computer, of course both eyes also blur lah!', 'You know how to diffentiate Left and Right meh?'],
    correct: 'You know how to diffentiate Left and Right meh?'
  }, {
    question: 'What is my new year resolution?', choices: ['变善良！！', 'Become Shorter', '马甲线 ^^', 'Work from home',
      'Sleep whole day', '被包养', 'All of the above'], correct: 'All of the above'
  }, {
    question: 'How pretty am I?', choices: ['Little', 'Very!', 'Prettier than Snow White', 'Prettier than you 就对了'],
    correct: 'Prettier than you 就对了'
  }
  ];

  constructor(private playerService: PlayerService, private fb: FormBuilder) {
    // this.createForm();
  }
  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.question = this.questions[this.number];
    this.getPlayers();
  }

  displayWrongMessage() {
    return this.wrongDisplay[Math.floor(Math.random() * this.wrongDisplay.length)];
  }

  onSelect(answer: string) {
    this.player.answers.push(answer);
    window.scroll(0, 0);
    if (answer === this.questions[this.number].correct) {
      this.totalMarks += 1;
    }
    this.number += 1;

    if (this.number < this.questions.length) {
      this.question = this.questions[this.number];
    } else {
      console.log('time used:', this.time);
      this.player.clearTime = this.time;
      clearInterval(this.interval);
      this.time = 0;
      // Ended
      if (this.totalMarks === this.questions.length) {
        this.player.result = 'Passed';
        this.question = { question: 'Congrats! You have got it all correct!', choices: [] };
      } else {
        this.displayMessage = this.wrongDisplay[Math.floor(Math.random() * this.wrongDisplay.length)];
        this.player.result = 'Failed';
        this.question = { question: 'Sorry! You did not pass the test!', choices: [] };
      }
      this.playerService.addPlayer(this.player).subscribe((res: any) => {
        console.log('results', res.result);
      });
    }
  }

  onRetry() {
    this.number = 0;
    this.player.result = '';
    this.player.answers = [];
    this.question = this.questions[this.number];
    this.totalMarks = 0;
    this.interval = setInterval(() => {
      this.time++;
    }, 1000);
  }

  onStart() {
    this.startGame = true;
    this.interval = setInterval(() => {
      this.time++;
    }, 1000);
  }

  getPlayers() {
    this.playerService.getAllPlayers().subscribe(
      (res: any) => {
        this.allPlayers = res.result;
      },
      err => {
        console.error(err);
      }
    );
  }

}
